package com.greedy.erp.board.repository;


import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.greedy.erp.board.entity.Board;
import com.greedy.erp.board.entity.Board;
import com.greedy.erp.regist.entity.Emp;

public interface BoardRepository extends JpaRepository<Board, Integer>  {


	@Query(value="SELECT * FROM BOARD WHERE EMP_CODE = :empCode ORDER BY BOARD_CODE DESC", nativeQuery = true)
	List<Board> findByEmpCode(@Param("empCode") int empCode);




	@Query(value="SELECT * FROM BOARD WHERE EMP_CODE = :empCode ORDER BY BOARD_CODE DESC", nativeQuery = true)
	List<Board> findByEmpCode(int empCode, Pageable paging);



//	@Query(value= "SELECT * FROM BOARD A \r\n"
//			+ "LEFT JOIN EMP B\r\n"
//			+ " ON (A.EMP_CODE = B.EMP_CODE)\r\n"
//			+ " WHERE A.EMP_CODE = 1", nativeQuery = true)
//	List<Board> findValidByEmpCode(@Param("empCode")int empCode);
//
//	@Query(value= "SELECT * FROM BOARD A \r\n"
//			+ "LEFT JOIN EMP B\r\n"
//			+ " ON (A.EMP_CODE = B.EMP_CODE)\r\n"
//			+ " WHERE A.EMP_CODE = 1", nativeQuery = true)
//	List<Board> findValidByEmpCode(Pageable paging, @Param("empCode")int empCode);


}
