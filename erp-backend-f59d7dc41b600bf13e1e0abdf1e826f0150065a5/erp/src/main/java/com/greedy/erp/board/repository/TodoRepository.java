package com.greedy.erp.board.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.greedy.erp.board.entity.Todo;
import com.greedy.erp.regist.entity.Emp;

public interface TodoRepository extends JpaRepository<Todo, Integer> {
	






	List<Todo> findByEmp(Emp emp);
	
	

	@Query(value="SELECT * FROM TODO WHERE EMP_CODE = :empCode ORDER BY TODO_CODE DESC", nativeQuery = true)
	List<Todo> findByEmpCode(@Param("empCode") int empCode, Pageable paging);

	



}
