package com.greedy.erp.production.account.dto;

import java.util.Date;

import com.greedy.erp.production.business.dto.SalesDTO;

public class DepositDTO {
	private Long depositCode;
	private Date depositDate;
	private Long depositAmount;
	private String depositNote;
	private SalesDTO salesCode;
	
	public DepositDTO() {}

	public DepositDTO(Long depositCode, Date depositDate, Long depositAmount, String depositNote, SalesDTO salesCode) {
		super();
		this.depositCode = depositCode;
		this.depositDate = depositDate;
		this.depositAmount = depositAmount;
		this.depositNote = depositNote;
		this.salesCode = salesCode;
	}

	public Long getDepositCode() {
		return depositCode;
	}

	public void setDepositCode(Long depositCode) {
		this.depositCode = depositCode;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

	public Long getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(Long depositAmount) {
		this.depositAmount = depositAmount;
	}

	public String getDepositNote() {
		return depositNote;
	}

	public void setDepositNote(String depositNote) {
		this.depositNote = depositNote;
	}

	public SalesDTO getSalesCode() {
		return salesCode;
	}

	public void setSalesCode(SalesDTO salesCode) {
		this.salesCode = salesCode;
	}

	@Override
	public String toString() {
		return "DepositDTO [depositCode=" + depositCode + ", depositDate=" + depositDate + ", depositAmount="
				+ depositAmount + ", depositNote=" + depositNote + ", salesCode=" + salesCode + "]";
	}
	
}