package com.greedy.erp.production.account.dto;

import java.util.Date;

import com.greedy.erp.production.purchase.dto.PurchaseDTO;

public class WithdrawDTO {
	private Long WithdrawCode;
	private Date WithdrawDate;
	private Long WithdrawAmount;
	private PurchaseDTO Purchase;
	private String WithdrawNote;
	
	public WithdrawDTO() {}

	public WithdrawDTO(Long withdrawCode, Date withdrawDate, Long withdrawAmount, PurchaseDTO purchase,
			String withdrawNote) {
		super();
		this.WithdrawCode = withdrawCode;
		this.WithdrawDate = withdrawDate;
		this.WithdrawAmount = withdrawAmount;
		this.Purchase = purchase;
		this.WithdrawNote = withdrawNote;
	}

	public Long getWithdrawCode() {
		return WithdrawCode;
	}

	public void setWithdrawCode(Long withdrawCode) {
		WithdrawCode = withdrawCode;
	}

	public Date getWithdrawDate() {
		return WithdrawDate;
	}

	public void setWithdrawDate(Date withdrawDate) {
		WithdrawDate = withdrawDate;
	}

	public Long getWithdrawAmount() {
		return WithdrawAmount;
	}

	public void setWithdrawAmount(Long withdrawAmount) {
		WithdrawAmount = withdrawAmount;
	}

	public PurchaseDTO getPurchase() {
		return Purchase;
	}

	public void setPurchase(PurchaseDTO purchase) {
		Purchase = purchase;
	}

	public String getWithdrawNote() {
		return WithdrawNote;
	}

	public void setWithdrawNote(String withdrawNote) {
		WithdrawNote = withdrawNote;
	}

	@Override
	public String toString() {
		return "WithdrawDTO [WithdrawCode=" + WithdrawCode + ", WithdrawDate=" + WithdrawDate + ", WithdrawAmount="
				+ WithdrawAmount + ", Purchase=" + Purchase + ", WithdrawNote=" + WithdrawNote + "]";
	}

	
	
}