package com.greedy.erp.production.account.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.greedy.erp.production.business.entity.Sales;

@Entity
@Table(name = "DEPOSIT")
@SequenceGenerator(
		name = "DEPOSIT_SEQ_GENERATOR",
		sequenceName = "SEQ_DEPOSIT_CODE",
		initialValue = 1, allocationSize = 1
		)
public class Deposit {
	
	@Id
	@Column( name = "DEPOSIT_CODE")
	private Long depositCode;
	
	@Column( name = "DEPOSIT_DATE")
	private Date depositDate;
	
	@Column( name = "DEPOSIT_AMOUNT")
	private Long depositAmount;
	
	@Column( name = "DEPOSIT_NOTE")
	private String depositNote;
	
	@ManyToOne
	@JoinColumn( name = "SALES_CODE")
	private Sales sales;
	
	public Deposit() {}

	public Deposit(Long depositCode, Date depositDate, Long depositAmount, String depositNote, Sales sales) {
		super();
		this.depositCode = depositCode;
		this.depositDate = depositDate;
		this.depositAmount = depositAmount;
		this.depositNote = depositNote;
		this.sales = sales;
	}

	public Long getDepositCode() {
		return depositCode;
	}

	public void setDepositCode(Long depositCode) {
		this.depositCode = depositCode;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

	public Long getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(Long depositAmount) {
		this.depositAmount = depositAmount;
	}

	public String getDepositNote() {
		return depositNote;
	}

	public void setDepositNote(String depositNote) {
		this.depositNote = depositNote;
	}

	public Sales getSales() {
		return sales;
	}

	public void setSales(Sales sales) {
		this.sales = sales;
	}

	@Override
	public String toString() {
		return "Deposit [depositCode=" + depositCode + ", depositDate=" + depositDate + ", depositAmount="
				+ depositAmount + ", depositNote=" + depositNote + ", sales=" + sales + "]";
	}
	
		
}