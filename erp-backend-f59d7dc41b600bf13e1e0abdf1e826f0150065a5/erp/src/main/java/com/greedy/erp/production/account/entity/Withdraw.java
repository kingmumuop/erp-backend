package com.greedy.erp.production.account.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import com.greedy.erp.production.purchase.entity.Purchase;

@Entity
@Table(name = "WITHDRAW")
@SequenceGenerator(
		name = "WITHDRAW_SEQ_GENERATOR",
		sequenceName = "SEQ_WITHDRAW_CODE",
		initialValue = 1, allocationSize = 1
		)
public class Withdraw {
	
	@Id
	@Column( name = "WITHDRAW_CODE")
	private Long WithdrawCode;
	
	@Column( name = "WITHDRAW_DATE")
	private Date WithdrawDate;
	
	@Column( name = "WITHDRAW_AMOUNT")
	private Long WithdrawAmount;
	
	@ManyToOne
	@JoinColumn( name = "PURCHASE_CODE")
	private Purchase purchase;
	
	@Column( name = "WITHDRAW_NOTE")
	private String WithdrawNote;

	public Withdraw() {}
	
	public Withdraw(Long withdrawCode, Date withdrawDate, Long withdrawAmount, Purchase purchase, String withdrawNote) {
		super();
		this.WithdrawCode = withdrawCode;
		this.WithdrawDate = withdrawDate;
		this.WithdrawAmount = withdrawAmount;
		this.purchase = purchase;
		this.WithdrawNote = withdrawNote;
	}

	public Long getWithdrawCode() {
		return WithdrawCode;
	}

	public void setWithdrawCode(Long withdrawCode) {
		WithdrawCode = withdrawCode;
	}

	public Date getWithdrawDate() {
		return WithdrawDate;
	}

	public void setWithdrawDate(Date withdrawDate) {
		WithdrawDate = withdrawDate;
	}

	public Long getWithdrawAmount() {
		return WithdrawAmount;
	}

	public void setWithdrawAmount(Long withdrawAmount) {
		WithdrawAmount = withdrawAmount;
	}

	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}

	public String getWithdrawNote() {
		return WithdrawNote;
	}

	public void setWithdrawNote(String withdrawNote) {
		WithdrawNote = withdrawNote;
	}

	@Override
	public String toString() {
		return "Withdraw [WithdrawCode=" + WithdrawCode + ", WithdrawDate=" + WithdrawDate + ", WithdrawAmount="
				+ WithdrawAmount + ", purchase=" + purchase + ", WithdrawNote=" + WithdrawNote + "]";
	}
	
	
	
	
}