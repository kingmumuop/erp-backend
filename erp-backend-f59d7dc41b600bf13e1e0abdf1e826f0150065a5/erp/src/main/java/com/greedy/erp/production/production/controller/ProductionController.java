package com.greedy.erp.production.production.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.greedy.erp.common.Criteria;
import com.greedy.erp.common.PageDTO;
import com.greedy.erp.common.PagingResponseDTO;
import com.greedy.erp.common.ResponseDTO;
import com.greedy.erp.production.production.service.ProductionService;
import com.greedy.erp.regist.dto.EmpDTO;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api/v1")
public class ProductionController {

	private static final Logger log = LoggerFactory.getLogger(ProductionController.class);
	private final ProductionService productionService;
	
	
	@Autowired
	public ProductionController(ProductionService productionService) {
		this.productionService = productionService;
	}
	
//	// 로그인한 사원 1명 정보 조회
//	@Operation(summary = "사원 1명 조회", description = "사원 1명 조회", tags = { "EmployeeController" })
//	@GetMapping("/select/{empCode}")
//	public ResponseEntity<ResponseDTO> selectEmpByEmpCode(@PathVariable Integer empCode) {
//		log.info("[EmployeeController] 넘어온 empCode 출력 : " + empCode);
//		EmpDTO empDTO = (EmpDTO)employeeService.findEmpByEmpCode(empCode);
//		return ResponseEntity.ok().body(new ResponseDTO(HttpStatus.OK, "사원 1명 조회 성공", empDTO));	
//	}
	
	@Operation(summary = "작업 목록 조회", description = "작업 목록 조회", tags = { "WorkController" })
	@GetMapping("/works")
	public ResponseEntity<ResponseDTO> selectEmployeeList(@RequestParam(name = "offset", defaultValue = "1") String offset) {
		log.info("[WorkController] 입력받은 offset 출력 : " + offset);
		
		int total = productionService.selectEmployeeTotal();
		log.info("[WorkController] total 값 출력 : " + total);
		
		Criteria cri = new Criteria(Integer.valueOf(offset), 10);
		PagingResponseDTO pagingResponseDTO = new PagingResponseDTO();
		pagingResponseDTO.setData(productionService.selectWorkListWithPaging(cri));	
		pagingResponseDTO.setPageInfo(new PageDTO(cri, total));
		
		return ResponseEntity.ok().body(new ResponseDTO(HttpStatus.OK, "작업 목록 조회 성공", pagingResponseDTO));	
	}
	
//	@Operation(summary = "사원 등록", description = "사원 등록 진행", tags = { "EmployeeController" })
//    @PostMapping(value = "/employees")
//    public ResponseEntity<ResponseDTO> insertEmployee(@RequestBody EmpDTO empDTO) {
//    	return ResponseEntity.ok().body(new ResponseDTO(HttpStatus.OK, "창고 등록 성공",  employeeService.insertEmployee(empDTO)));
//    }
//	
//	@Operation(summary = "사원 수정", description = "사원 수정 진행", tags = { "EmployeeController" })
//    @PutMapping(value = "/employees")
//    public ResponseEntity<ResponseDTO> modifyEmployee(@RequestBody EmpDTO empDTO) {
//    	return ResponseEntity.ok().body(new ResponseDTO(HttpStatus.OK, "사원 수정 성공",  employeeService.modifyEmployee(empDTO)));
//    }
	
}
