package com.greedy.erp.production.production.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.greedy.erp.regist.entity.Product;
import com.greedy.erp.regist.entity.Storage;

@Entity
@Table(name = "WORKDETAIL")
public class WorkDetail {
	
	@Id
	@Column(name = "WORK_NO")
	private int workNo;
	
	@Column(name = "PRODUCTION_QUANTITY")
	private int productionQuantity;
	
	@Column(name = "CONSUMPTION_PER_INGREDIENT")
	private int consumptionPerIngredient;
	
	@Column(name = "WORK_NOTE")
	private String workNote;
	
	@Column(name = "WORK_CODE")
	private int workCode;
	
	@Column(name = "PRODUCT_CODE")
	private int ingredientCode;
	
	@Column(name = "STORAGE_CODE")
	private int storageCode;
	
	@ManyToOne
	@JoinColumn(name = "PRODUCT_CODE", insertable = false, updatable = false)
	private Product ingredient;
	
	@ManyToOne
	@JoinColumn(name = "STORAGE_CODE", insertable = false, updatable = false)
	private Storage storage;

	public WorkDetail() {
		super();
	}

	public WorkDetail(int workNo, int productionQuantity, int consumptionPerIngredient, String workNote, int workCode,
			int ingredientCode, int storageCode, Product ingredient, Storage storage) {
		super();
		this.workNo = workNo;
		this.productionQuantity = productionQuantity;
		this.consumptionPerIngredient = consumptionPerIngredient;
		this.workNote = workNote;
		this.workCode = workCode;
		this.ingredientCode = ingredientCode;
		this.storageCode = storageCode;
		this.ingredient = ingredient;
		this.storage = storage;
	}

	public int getWorkNo() {
		return workNo;
	}

	public void setWorkNo(int workNo) {
		this.workNo = workNo;
	}

	public int getProductionQuantity() {
		return productionQuantity;
	}

	public void setProductionQuantity(int productionQuantity) {
		this.productionQuantity = productionQuantity;
	}

	public int getConsumptionPerIngredient() {
		return consumptionPerIngredient;
	}

	public void setConsumptionPerIngredient(int consumptionPerIngredient) {
		this.consumptionPerIngredient = consumptionPerIngredient;
	}

	public String getWorkNote() {
		return workNote;
	}

	public void setWorkNote(String workNote) {
		this.workNote = workNote;
	}

	public int getWorkCode() {
		return workCode;
	}

	public void setWorkCode(int workCode) {
		this.workCode = workCode;
	}

	public int getIngredientCode() {
		return ingredientCode;
	}

	public void setIngredientCode(int ingredientCode) {
		this.ingredientCode = ingredientCode;
	}

	public int getStorageCode() {
		return storageCode;
	}

	public void setStorageCode(int storageCode) {
		this.storageCode = storageCode;
	}

	public Product getIngredient() {
		return ingredient;
	}

	public void setIngredient(Product ingredient) {
		this.ingredient = ingredient;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	@Override
	public String toString() {
		return "WorkDetail [workNo=" + workNo + ", productionQuantity=" + productionQuantity
				+ ", consumptionPerIngredient=" + consumptionPerIngredient + ", workNote=" + workNote + ", workCode="
				+ workCode + ", ingredientCode=" + ingredientCode + ", storageCode=" + storageCode + ", ingredient="
				+ ingredient + ", storage=" + storage + "]";
	}
	
	
	
}
