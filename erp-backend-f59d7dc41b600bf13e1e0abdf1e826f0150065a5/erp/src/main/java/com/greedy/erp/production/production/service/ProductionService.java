package com.greedy.erp.production.production.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.greedy.erp.common.Criteria;
import com.greedy.erp.production.production.dto.WorkDTO;
import com.greedy.erp.production.production.entity.Work;
import com.greedy.erp.production.production.repository.WorkRepository;
import com.greedy.erp.regist.dto.EmpDTO;
import com.greedy.erp.regist.entity.Emp;

@Service
public class ProductionService {
	
	private static final Logger log = LoggerFactory.getLogger(ProductionService.class);
	private final ModelMapper modelMapper;
	private final WorkRepository workRepository;
	
    
	@Autowired
	public ProductionService(ModelMapper modelMapper, WorkRepository workRepository) {
		this.modelMapper = modelMapper;
		this.workRepository = workRepository;
	}
	
	/* 전체 작업 조회하기 */
	public Object selectWorkListWithPaging(Criteria cri) {
		log.info("[WorkService] selectWorkListWithPaging Start ==========");
		
		int index = cri.getPageNum() - 1;
	    int count = cri.getAmount(); 
	    Pageable paging = PageRequest.of(index, count, Sort.by("workCode").descending());
	    Page<Work> result = workRepository.findAll(paging);
	    List<Work> workList = (List<Work>)result.getContent();
	    
	    log.info("[WorkService] selectWorkListWithPaging End ============");
    
    return workList.stream().map(work -> modelMapper.map(work, WorkDTO.class)).collect(Collectors.toList());
	}
	
	public int selectEmployeeTotal() {
		log.info("[EmployeeService] selectEmployeeTotal Start ==========");
		List<Work> empList = workRepository.findAll();
		log.info("[EmployeeService] selectEmployeeTotal End ==========");
		
		return empList.size();
	}

//	/* 작업 등록 */
//	@Transactional
//	public Object insertEmployee(EmpDTO empDTO) {
//		log.info("[EmployeeService] ===== insertEmployee Start =====");
//		log.info("[EmployeeService] EmpDTO : " + empDTO);
//        
//		int result = 0;
//		try {	
//			Emp emp = modelMapper.map(empDTO, Emp.class); 
//			
//			employeeRepository.save(emp);
//			
//			result = 1;
//		} catch (Exception e) {
//			log.info("[EmployeeService] exception");
//		}
//		
//		log.info("[EmployeeService] ====== insertEmployee End ======");
//		return (result > 0)? "등록 성공" : "등록 실패";
//		}
//
//	/* 작업 수정 */
//	@Transactional
//	public Object modifyEmployee(EmpDTO empDTO) {
//		log.info("[EmployeeService] ===== modifyEmployee Start =====");
//		log.info("[EmployeeService] EmpDTO : " + empDTO);
//        
//		int result = 0;
//		try {	
//			Emp emp = modelMapper.map(empDTO, Emp.class); 
//			
//			employeeRepository.save(emp);
//			
//			result = 1;
//		} catch (Exception e) {
//			log.info("[EmployeeService] exception");
//		}
//		
//		log.info("[EmployeeService] ====== modifyEmployee End ======");
//		return (result > 0)? "수정 성공" : "수정 실패";
//		}

}
