package com.greedy.erp.task.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.greedy.erp.task.dto.ApprovalLineDTO;
import com.greedy.erp.task.dto.JoinedApprovalDTO;
import com.greedy.erp.task.entity.Approval;
import com.greedy.erp.task.entity.ApprovalLine;
import com.greedy.erp.task.repository.ApprovalLineRepository;
import com.greedy.erp.task.repository.ApprovalRepository;

@Service
public class ApprovalLineService {

	private static final Logger log = LoggerFactory.getLogger(ApprovalLineService.class);
	private final ApprovalLineRepository approvalLineRepository;
	private final ApprovalRepository approvalRepository;
	private final ModelMapper modelMapper;
	
    @Value("${image.image-dir}")
    private String IMAGE_DIR;
    @Value("${image.image-url}")
    private String IMAGE_URL;
    
	@Autowired
    public ApprovalLineService(ApprovalLineRepository approvalLineRepository, ApprovalRepository approvalRepository, ModelMapper modelMapper) {
		this.approvalLineRepository = approvalLineRepository;
		this.approvalRepository = approvalRepository;
		this.modelMapper = modelMapper;
	}

	/* 결재코드로 라인 조회 */
	public Object selectApprovalLineByApproval(int approvalCode) {
		List<ApprovalLine> approvalLineList = approvalLineRepository.findByApprovalCodeOrderByApproverOrderAsc(approvalCode);
		
		for (int i = 0; i < approvalLineList.size(); i++) {
			approvalLineList.get(i).getEmp().setEmpStamp(IMAGE_URL + "stampimgs/" + approvalLineList.get(i).getEmp().getEmpStamp());
		}
		
		return approvalLineList.stream().map(approvalLine -> modelMapper.map(approvalLine, ApprovalLineDTO.class)).collect(Collectors.toList());
	}
	
	/* 승인여부 변경 */
	@Transactional
	public Object updateApproveYn(int empCode, int approvalCode) {
		ApprovalLine approvalLine = approvalLineRepository.updateApproveYn(empCode, approvalCode);
		approvalLine.setApproveYn("Y");
		approvalLine.setApprovedDate(new Date(System.currentTimeMillis()));
		log.info("★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★");

		boolean answer = approvalLine.getFinalYn() == "Y";
		log.info(approvalLine.getFinalYn());
		log.info(answer + "");
		
		if("Y".equals(approvalLine.getFinalYn())) {
			Approval approval = approvalRepository.findById(approvalCode).get();
			approval.setStatusCode(3);
		} else {
			Approval approval = approvalRepository.findById(approvalCode).get();
			approval.setStatusCode(1);
		}
		return modelMapper.map(approvalLine, ApprovalLineDTO.class);
	}


}
