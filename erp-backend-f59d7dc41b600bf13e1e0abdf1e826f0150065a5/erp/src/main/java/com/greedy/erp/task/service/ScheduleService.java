package com.greedy.erp.task.service;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greedy.erp.task.repository.ScheduleRepository;

@Service	
public class ScheduleService {

	private static final Logger log = LoggerFactory.getLogger(ScheduleService.class);
	private final ScheduleRepository scheduleRepository;
	private final ModelMapper modelMapper;
	
	@Autowired
    public ScheduleService(ScheduleRepository scheduleRepository, ModelMapper modelMapper) {
		this.scheduleRepository = scheduleRepository;
		this.modelMapper = modelMapper;
	}
}
